# Coding Manual

## Server

### Controllers

Controllers are defined at `controllers/` folder.
Controllers are defined by a static class and each handle are defined by a function of it.

```typescript
// controllers/person/person.controller.ts

export class PersonController extends ResourceController {
  // ...more code
  static store(person) {
    // Do the trick!
  }
}    
```

#### Testing
Each controller must to have a test named by `<controller_name>.test.ts` and it must be grouped in a folder.
The main test runner is Jest, and you can run your test scripts using `npm run server:test`
or `npm run server:test --testNamePattern "PersonController"` to run a specific one.

```typescript
// controllers/person/person.test.ts
import { PersonController } from './person.controller';

describe('PersonController', () => {
  // Test it here!
});
```

### Routing

The app routes are defined at `routes.ts` file in a `Route` array.
By example, an HTTP route can be defined by:
```typescript
// routes.ts

// ...more code
{ method: 'POST', endpoint: '/person', handler: PersonController.register }
```
This routes will point to `/api/person` path. The `api` prefix is added by default.
To change or remove the prefix, see the `app.ts` module.

### Validating Parameters

#### Declaring
Validations can be defined at `controllers/<controller_name>/<controller_name>.params.ts`.
A param is a class definition used to specify the Schema validation of the param of a controller function.
```typescript
// controllers/person/person.params.ts

import { schema } from '../../utils/schematize';

export class PersonParams {
  @schema({
    type: 'string',
    required: true,
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    required: true,
  })
  age: number;
}
```

The `@schema` is a decorator function that creates a schema utilizing the [JSONSchema](https://spacetelescope.github.io/understanding-json-schema/index.html) specification.
Nesting schemas are supported too using the `$ref` option.

#### Applying

Validations can be applied in a controller function using the `@validate` decorator.
This decorator will wrap the target function with a validator that check the param using the schema.
```typescript
// controllers/person/person.controller.ts

export class PersonController extends ResourceController {
  // ...more code
  @validate(PersonParams)
  static store(person: PersonParams) {
    // Do the trick!
  }
}
```
If the param is valid, it run the target function normally. If not, it throws an `IllegalArgumentError`
. In a express server, `IllegalArgumentError`s are converted to a `BadRequestError`.

#### Testing a Validation Error
Surrounding the function call with a `try-catch` statement, it possible to extract the error and assert by it properties.
Each item in `error.errors` is a `ValidationError` instance from the [jsonschema](npmjs.com/package/jsonschema) package.

```typescript
// controllers/person/person.test.ts

describe('PersonController', () => {
  test('store: It should not store a person with a invalid age.', () => {
    try {
      PersonController.store({ name: 'Filipe', age: 110 });
    } catch (error: IllegalArgumentError) {
      expect(error.errors[0].property).toBe('instance.age');
    }
  });
});
```

### Creating Middlewares

#### Declaring

```typescript
// middlewares/authentication.ts

import { AuthenticationError } from '../utils/http/errors';
import { Request, Response } from 'express-serve-static-core';

export function isAuthenticated(req: Request, res: Response, next: Function) {
  if (req.headers.authorization === process.env.TOKEN_SECRET) return next();
  return res.status(401).send(new AuthenticationError('Unauthorized API Token.'));
}
```

