import { Validator } from 'jsonschema';

export const validator = new Validator();

// JSON Schema interface draft-07 with a mod to support
// `required` field as a boolean in the property
export interface Schema {
  id?: string;
  $schema?: string;
  $ref?: string;
  multipleOf?: number;
  maximum?: number;
  exclusiveMaximum?: boolean;
  minimum?: number;
  exclusiveMinimum?: boolean;
  maxLength?: number;
  minLength?: number;
  pattern?: string;
  additionalItems?: boolean | Schema;
  items?: Schema | Schema[];
  maxItems?: number;
  minItems?: number;
  uniqueItems?: boolean;
  maxProperties?: number;
  minProperties?: number;
  required?: boolean;
  additionalProperties?: boolean | Schema;
  'enum'?: any[];
  type?: string | string[];
  format?: string;
  allOf?: Schema[];
  anyOf?: Schema[];
  oneOf?: Schema[];
  not?: Schema;
}

function initSchema(target: any) {
  Object.defineProperty(target.constructor, '$schema', {
    enumerable: false,
    value: {
      id: '/' + target.constructor.name,
      type: 'object',
      properties: {},
      required: []
    }
  });
}

/* @TODO: Change it to custom AJV rule creator? */
function createRuleDecorator<T>(name: string, type?: any) {
  const decorator = function (config?: T): PropertyDecorator {
    return <PropertyDecorator>
      function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        if (!target.constructor.$schema) initSchema(target);
        if (!target.constructor.$schema.properties[propertyKey])
          target.constructor.$schema.properties[propertyKey] = {};

        if (!target.constructor.$schema.properties[propertyKey][type])
          target.constructor.$schema.properties[propertyKey]['type'] = type;

        target.constructor.$schema.properties[propertyKey][name] = config;

        validator.addSchema(target.constructor.$schema, target.constructor.$schema.id);
      };
  };
  return decorator;
}

export function schema(config: Schema): PropertyDecorator {
  return <PropertyDecorator>
    function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      if (!target.constructor.$schema) initSchema(target);
      if (!target.constructor.$schema.properties[propertyKey])
        target.constructor.$schema.properties[propertyKey] = {};

      const {
        required,
        ...restConfig
      } = config;

      if (required) target.constructor.$schema.required.push(propertyKey);

      target.constructor.$schema.properties[propertyKey] = {
        ...target.constructor.$schema[propertyKey],
        ...restConfig
      };

      validator.addSchema(target.constructor.$schema, target.constructor.$schema.id);
    };
}
