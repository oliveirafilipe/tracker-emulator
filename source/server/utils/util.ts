import { ValidationError } from 'jsonschema';

export async function iterate(iterable: Generator, iterator: Function, errorHandler?) {
  let streamFrame;
  let frame;

  if (!iterable.next)
    return iterator(iterable);

  do {
    try {
      streamFrame = iterable.next();
      frame = streamFrame;

      if (streamFrame instanceof Promise) {
        frame = await streamFrame;
        iterator(frame.value);
      } else iterator(frame.value);
    } catch (error) {
      if (errorHandler) errorHandler(error);
      else throw errorHandler;
    }

  } while (!frame.done);
}

export function toArrayBuffer(buffer): any[] {
  const array = [];
  for (let i = 0; i < buffer.length; i++) {
    array[i] = buffer[i];
  }
  return array;
}

export function toBufferArray(array): Buffer {
  const buffer = new Buffer(array.length);
  for (let i = 0; i < array.length; i++) {
    buffer[i] = array[i];
  }
  return buffer;
}

export class IllegalArgumentError extends Error {
  errors: ValidationError[];
  constructor(message, errors?) {
    super(message);
    this.errors = errors;
  }
}

export function pad(num, size): string {
  let s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}

export function formatString(format: string, ...args) {
  return format.replace(/{(\d+)}/g, function(match, number) {
    return typeof args[number] !== 'undefined'
      ? args[number]
      : match
      ;
  });
}
