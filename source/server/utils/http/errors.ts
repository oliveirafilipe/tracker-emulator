/**
 * Define an error that is used in controllers to error throwing.
 * @extends Error
 * @property name The type of the Error.
 * @property message The readable message of this error.
 * @property status The HTTP status of that error.
 * @property signal A default constant signal to use in the client side.
 */
import { ValidationError } from 'jsonschema';

export class ApiError extends Error {
  statusCode: number;
  signal: string;

  constructor(message: string, statusCode: number, signal: string) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.statusCode = statusCode;
    this.signal = signal;
    Error.captureStackTrace(this, this.constructor);
  }
}

export const UNAUTHORIZED = 'UNAUTHORIZED';
export class AuthenticationError extends ApiError {
  constructor(message: string) {
    super(message, 401, UNAUTHORIZED);
  }
}

export const NOT_FOUND = 'NOT_FOUND';
export class NotFoundError extends ApiError {
  constructor(message: string) {
    super(message, 404, NOT_FOUND);
  }
}

export const BAD_REQUEST = 'BAD_REQUEST';
export class BadRequestError extends ApiError {
  errors: ValidationError[];
  constructor(message: string, errors?: ValidationError[]) {
    super(message, 400, BAD_REQUEST);
    this.errors = errors;
  }
}

