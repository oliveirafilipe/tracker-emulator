export interface ResourceController {
  index: Function;
  create: Function;
  store: Function;
  show: Function;
  edit: Function;
  update: Function;
  destroy: Function;
}
