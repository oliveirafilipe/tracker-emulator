export class FloatRouding {

  public static ceil(value, precision) {
    return this._decimalAdjust( 'ceil', value, precision );
  }

  public static floor(value, precision) {
    return this._decimalAdjust( 'floor', value, precision );
  }

  public static round(value, precision) {
    return this._decimalAdjust( 'round', value, precision );
  }


  /**
   * Adjusts a number to a given precision, working around the buggy floating-point math of Javascript. Works for
   * round, floor, ceil operations.
   *
   * Lifted from the Math.round entry of MDN. Minor changes without effect on the algorithm.
   *
   * @param   {string} operation  "round", "floor", "ceil"
   * @param   {number} value
   * @param   {number} [precision=0]   can be negative: round( 104,-1 ) => 100
   * @returns {number}
   */
  private static _decimalAdjust ( operation, value, precision ) {
    let exp;

    if ( typeof precision === 'undefined' || +precision === 0 ) return Math[operation]( value );

    value = +value;
    precision = +precision;

    // Return NaN if the value is not a number or the precision is not an integer
    if ( isNaN( value ) || !(typeof precision === 'number' && precision % 1 === 0) ) return NaN;

    exp = -precision;

    // Shift
    value = value.toString().split( 'e' );
    value = Math[operation]( +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)) );

    // Shift back
    value = value.toString().split( 'e' );
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }
}
