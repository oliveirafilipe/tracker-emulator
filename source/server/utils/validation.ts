import { validator } from './schematize';
import { IllegalArgumentError } from './util';

export function validate(schemaClass: Function): PropertyDecorator {
  return <PropertyDecorator>
    function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      let originalHandler;

      function replaced(arg) {
        const validation = validator.validate(arg, schemaClass['$schema']);
        if (validation.errors.length > 0) {
          throw new IllegalArgumentError('Validation error.', validation.errors);
        }
        return originalHandler(arg);
      }

      if (descriptor) {
        originalHandler = descriptor.value;
        descriptor.value = replaced;
      } else {
        originalHandler = target[propertyKey];
        delete target[propertyKey];
        Object.defineProperty(target, propertyKey, {
          configurable: true,
          writable: true,
          enumerable: true,
          value: replaced
        });
      }
    };
}
