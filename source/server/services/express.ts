import express from 'express';
import compression from 'compression';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import * as httpStatus from 'http-status';
import { blue } from 'colors';

import { Debug } from '../utils/debug';
import { RouterService } from './router';
import { IllegalArgumentError } from '../utils/util';
import { BadRequestError } from '../utils/http/errors';

const debug = new Debug('services:express');

export interface ExpressServiceConfiguration {
  cors?: {
    origin?: string | Array<string> | Function;
    maxAge?: number;
    credentials?: boolean,
    exposedHeaders?: Array<string>;
    allowedHeaders?: Array<string>;
    methods?: Array<string>;
  };
  appLivePort?: number;
  morgan?: boolean;
  apiRoute?: string;
  staticRoute?: string;
}

export class ExpressService {
  static server: express.Application = express();

  static configure(config: ExpressServiceConfiguration) {

    if (config.morgan)
      ExpressService.server.use(morgan('dev'));

    if (config.apiRoute) {
      // parse application/x-www-form-urlencoded
      ExpressService.server.use(config.apiRoute + '*', bodyParser.urlencoded({'extended': true}));
      // parse application/json
      ExpressService.server.use(config.apiRoute + '*', bodyParser.json());
      // parse application/vnd.api+json as json
      ExpressService.server.use(config.apiRoute + '*', bodyParser.json({type: 'application/vnd.api+json'}));
      ExpressService.server.use(config.apiRoute + '*', cors(config.cors));
    }

    if (config.staticRoute) {
      // Handling Frontend livereload App routing
      if (config.appLivePort) {
        // Redirect to the angular's livereload server when running app in `npm serve`.
        ExpressService.server.get('/', function (req, res) {
          res.redirect('http://localhost:' + config.appLivePort);
        });
      } else {
        ExpressService.server.use(config.staticRoute, compression()); // Enabling GZip
        ExpressService.server.use(config.staticRoute, express.static(__dirname + '/../../www')); // Host app bundle on static source
        // Redirect GET / to static source
        ExpressService.server.get('/', function (req, res) {
          res.redirect(config.staticRoute);
        });
      }
    }

    RouterService.addMethod('GET', function (route) {
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      ExpressService.server.get(config.apiRoute + route.endpoint, route.middlewares, toRouteHandler(route.handler));
    });
    RouterService.addMethod('POST', function (route) {
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      ExpressService.server.post(config.apiRoute + route.endpoint, route.middlewares, toRouteHandler(route.handler));
    });
    RouterService.addMethod('PUT', function (route) {
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      ExpressService.server.put(config.apiRoute + route.endpoint, route.middlewares, toRouteHandler(route.handler));
    });
    RouterService.addMethod('DELETE', function (route) {
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      ExpressService.server.delete(config.apiRoute + route.endpoint, route.middlewares, toRouteHandler(route.handler));
    });
  }

  /**
   * Start the server on a Port.
   */
  static listen(port) {
    ExpressService.server.listen(port, () => {
      debug.info(`${new Date(Date.now())} Server started on ${port}.`);
      console.log(blue(`
      -------------------
      Hello NodeJs World!
      Port: ${port}
      -------------------`));
    });
  }
}

/**
 * Transform functions into a Express Middleware.
 * @param {Function|Generator} func The function in the functional paradigm that will be transformed into a Express Middleware.
 */
function toRouteHandler(func: Function): express.Handler {
  return async function (req, res) {
    try {
      const result = await func({ ...req.params, ...req.body, ...req.query }, req, res);
      if (typeof result === 'function') result(req, res);
      else {
        res
          .status(httpStatus.OK)
          .send(result);
      }
    } catch (error) {
      debug.error(error);
      if (error instanceof IllegalArgumentError) {
        error = new BadRequestError(error.message, error.errors);
      }
      res
        .status(error.statusCode || httpStatus.INTERNAL_SERVER_ERROR)
        .send({
          name: error.name,
          message: error.message,
          signal: error.signal || 'UNEXPECTED_INTERNAL_ERROR',
          errors: error.errors
        });
    }
  };
}
