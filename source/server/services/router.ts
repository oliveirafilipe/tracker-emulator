import { Debug } from '../utils/debug';
const debug = new Debug('services:router');

export interface Route {
  method: string;
  endpoint: string;
  handler: Function;
  middlewares?: any[];
  topic?: string;
  access?: 'CHANNEL' | 'BRODCAST' | 'PRIVATE';
}

export type RouteBootstrapper = (route: Route) => void;

export class RouterService {
  static methods = {};
  static addMethod(name: string, bootstrapper: RouteBootstrapper) {
    RouterService.methods[name] = bootstrapper;
  }

  static setup(routes: Route[]) {
    routes.forEach(route => {
      // Preventing undefined value
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      debug.log(`Routing ${route.method} ${route.endpoint} to ${route.handler.name}.`);
      RouterService.methods[route.method](route);
    });
  }
}
