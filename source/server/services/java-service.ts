import java from 'java';
import glob from 'glob';

export class JavaService {

  public static configure() {
    const files = glob.sync(__dirname + '/../modules/**/*.jar');
    for (const file of files) {
      console.log(file);
      java.classpath.push(file);
    }
  }

  public static getJavaInstance() {
    return java;
  }

}
