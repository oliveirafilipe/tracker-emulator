import { createSocket, Socket } from 'dgram';
import { Debug } from '../utils/debug';

const endpoints = {
  rst: {
    port: 7060,
    host: 'localhost'
  }
};

export class UdpClientService {
  private static client: Socket;
  private static debug: Debug = new Debug('clients:udp');


  public static getClient(): Socket {
    if (this.client == null) {
      this.client = createSocket('udp4');
    }
    return this.client;
  }

  public static sendMessage(vendor: string, messageBuffer: Buffer): boolean {
    if (!endpoints[vendor]) throw Error('Undefined vendor');

    this.getClient();
    try {
      this.client.send(messageBuffer, 0, messageBuffer.length, endpoints[vendor].port, endpoints[vendor].host, (err, bytes) => {
        if (err) throw err;
      });
      this.debug.log(messageBuffer.toString('hex'));
    } catch (e) {
      this.debug.error('Failed to Send UDP Packet for ', vendor);
      return false;
    }
    return true;
  }


}
