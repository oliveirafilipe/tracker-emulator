import socketIO from 'socket.io';
import { iterate } from '../utils/util';
import { Debug } from '../utils/debug';
import { RouterService, Route } from './router';

const debug = new Debug('services:socket');

export interface SocketModuleConfiguration {
  apiRoute?: string;
  wildcard?: string;
}

export class WebSocketService {
  static server: socketIO.Server;

  static configure(config: SocketModuleConfiguration) {
    WebSocketService.server = socketIO();
    WebSocketService.server.attach(0);
    WebSocketService.server.origins(config.wildcard || '*:*');

    RouterService.addMethod('WS', function(route) {
      const namespace = WebSocketService.server.of(config.apiRoute + route.endpoint);

      // Setup Socket middlewares
      route.middlewares.forEach((middleware) => {
        namespace.use((socket, next) => {
          middleware(socket.request, socket, next);
        });
      });

      namespace.on('connection', toSocketEmitter(<GeneratorFunction> route.handler, route.topic, route.access));
    });
  }

  static listen(port: any) {
    WebSocketService.server.listen(port);
  }
}

function secureEmit(socket, topic, data, access?) {
  if (data)
    switch (access) {
      case 'CHANNEL':
        socket.broadcast.emit(topic, data);
        socket.emit(topic, data.value);
        break;
      case 'BROADCAST':
        socket.broadcast.emit(topic, data);
        break;
      case 'PRIVATE':
      default:
        socket.emit(topic, data);
        break;
    }
}

/**
 * Transform a iterator into a socket emitter.
 * @param {GeneratorFunction} func
 * @returns {Function} A socket handler.
 */
function toSocketEmitter(func: GeneratorFunction, topic: string, access?: string): Function {
  return function(socket) {
    debug.log('New WS connection:', topic);

    const connIterator = func();
    iterate(connIterator,
      value => secureEmit(socket, topic, value, access),
      error => secureEmit(socket, topic, { error: error.message }, access)
    );

    socket.on(topic, async (message) => {
      debug.log('New WS message:', topic, message);

      const iterator = func(message);
      iterate(iterator,
        value => secureEmit(socket, topic, value, access),
        error => secureEmit(socket, topic, { error: error.message }, access)
      );
    });
  };
}
