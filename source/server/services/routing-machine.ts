import Axios from 'axios';
import * as polyline from '@mapbox/polyline';
import { Coordinate } from '../types';
import {Debug} from '../utils/debug';

export class RoutingMachine {
  private static HOST = 'http://router.project-osrm.org';
  private static readonly debug: Debug = new Debug('routing-machine');

  public static async route(coordinates: number[][]): Promise<Coordinate[]> {
    let coordinatesParam = '';
    for (let i = 0; i < coordinates.length; i++) {
      coordinatesParam += coordinates[i].join(',') + ';';
    }
    coordinatesParam = coordinatesParam.slice(0, -1);
    try {
      const url = this.HOST + '/route/v1/driving/' + coordinatesParam + '?steps=true';
      this.debug.log(url);
      const { data } = await Axios.get(url);

      const maneuverPoints = this.getManeuverPoints(data.routes[0].legs[0].steps);
      const allPoints = polyline.decode(data.routes[0].geometry);

      return this.mergePoints(allPoints, maneuverPoints);

    } catch (e) {
      this.debug.error(e);
    }

    return [];
  }

  private static getManeuverPoints(routeSteps: any[]): Array<Coordinate> {
    const maneuverPoints: Coordinate[] = [];
    for (let i = 0; i < routeSteps.length; i++) {
      maneuverPoints.push({
        longitude: parseFloat(routeSteps[i].maneuver.location[0].toFixed(5)),
        latitude: parseFloat(routeSteps[i].maneuver.location[1].toFixed(5)),
        bearing: routeSteps[i].maneuver.bearing_after
      });
    }

    return maneuverPoints;
  }

  private static mergePoints(routePoints: number[], maneuverPoints: Coordinate[]): Coordinate[] {
    let i = 0;
    const mergedPoints: Array<Coordinate> = [];
    for (const location of routePoints) {
      if (maneuverPoints[i].latitude === location[0] && maneuverPoints[i].longitude === location[1]) {
        mergedPoints.push(maneuverPoints[i++]);
      } else {
        mergedPoints.push({ bearing: maneuverPoints[i - 1].bearing, ...maneuverPoints[i] });
      }
    }

    return mergedPoints;
  }

}
