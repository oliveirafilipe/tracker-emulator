import { RoutingMachine } from './routing-machine';
import { Concox } from '../modules/concox';
import { Rst } from '../modules/rst';

export class Emulator {

  static async start() {

    // TRACKTUS - EXPOINTER
    // const seed = [[-51.168261,-29.843860],[-51.180510,-29.854422]];

    // TRACKTUS - PUCRS
    // const seed = [[-51.168261,-29.843860],[-51.174521,-30.057237]];

    // R. Rio do Padro, Esteio - Expointer
    const seed = [[-51.162320, -29.840056], [-51.180510, -29.854422]];

    const coordinates = (await RoutingMachine.route(seed));

    Concox.sendPosition({
      deviceId: '351608081052107',
      interval: 4,
      ignitionOnPercent: 0,
      validPercent: 0.5,
      memoryPercent: 0.5
    }, coordinates);

    Concox.sendPosition({
      deviceId: '351608001052107',
      interval: 6,
      ignitionOnPercent: 0,
      validPercent: 0.5,
      memoryPercent: 0.5
    }, coordinates);

    Rst.sendPosition({
      deviceId: '014062750',
      interval: 8,
      ignitionOnPercent: 0,
      validPercent: 0.5,
      memoryPercent: 0.5
    }, coordinates);

  }

}
