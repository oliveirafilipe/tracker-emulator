import { Pool } from 'pg';
import { Debug } from '../utils/debug';

const debug = new Debug('services:pg');

export class PostgresqlService {
  static pool: Pool;

  static connect(postgresURL, maxConnections) {
    debug.log('Connecting', postgresURL);

    PostgresqlService.pool = new Pool({
      connectionString: postgresURL,
      max: maxConnections,
    });

    PostgresqlService.pool.on('error', (err) => {
      debug.error('Unexpected error in PG client.', err);
    });
  }

  static async getClient() {
    return await PostgresqlService.pool.connect();
  }
}
