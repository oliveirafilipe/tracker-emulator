import { Socket } from 'net';
import { promisify } from 'util';
import { Debug } from '../utils/debug';

const endpoints = {
  concox: {
    port: 7000,
    host: 'tracktus.ddns.net'
  }
};

export class TcpClientService {
  private static clients: Array<Socket> = [];
  private static connected = false;
  private static readonly debug: Debug = new Debug('clients:tcp');


  public static async getClient(vendor): Promise<Socket> {
    if (!endpoints[vendor]) throw Error('Undefined vendor');

    if (this.clients[vendor] == null) {
      this.clients[vendor] = new Socket();
      await promisify(this.clients[vendor].connect).bind(this.clients[vendor])(endpoints[vendor].port, endpoints[vendor].host);
      this.connected = true;
    }
    return this.clients[vendor];
  }

  public static async sendMessage(vendor: string, messageBuffer: Buffer): Promise<boolean> {
    const client: Socket = await this.getClient(vendor);
    try {
      client.write(messageBuffer);
      this.debug.log(messageBuffer.toString('hex'));
    } catch (e) {
      this.debug.error('Error Sendind TCP Packet for ', vendor);
      return false;
    }
    return true;
  }

}
