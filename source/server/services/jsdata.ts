import { Container, Mapper, Query, Record, Schema } from 'js-data';
import { schema, validator } from '../utils/schematize';

export class Jsdata {
  static store = new Container();
}

interface RelationDescription {
  [key: string]: {
    localKeys?: string;
    foreignKeys?: string;
    foreignKey?: string;
    localField: string;
  };
}

export interface ModelOptions {
  relations?: {
    belongsTo?: RelationDescription
    hasMany?: RelationDescription
    hasOne?: RelationDescription
  };
}

// declare type ClassDecorator = <T>(target: { new(...args: any[]): T}) => T;
// export interface ModelInterface<T> {
//   new(props: T): T;
//   destroy(id: string|number, opts?: any): Promise<any>;
//   destroyAll(query: Query, opts?: any): Promise<any>;
//   find(id: string|number, opts?: any): Promise<T>;
//   findAll(query: Query, opts?: any): Promise<T[]>;
//   update(id: string|number, props: any, opts?: any): Promise<any>;
//   updateAll(props: any, query: Query, opts?: any): Promise<any>;
// }

export class Model<T> {
  constructor(props: T) {
    Object.keys(props).forEach(key => {
      this[key] = props[key];
    });
  }
  static destroy(id: string|number, opts?: any): Promise<any> { return new Promise<any>(() => {}); }
  static destroyAll(query: Query, opts?: any): Promise<any> { return new Promise<any>(() => {}); }
  static find<T>(id: string|number, opts?: any): Promise<T> { return new Promise<T>(() => {}); }
  static findAll<T>(query: Query, opts?: any): Promise<T[]> { return new Promise<T[]>(() => {}); }
  static update(id: string|number, props: any, opts?: any): Promise<any> { return new Promise<any>(() => {}); }
  static updateAll(props: any, query: Query, opts?: any): Promise<any> { return new Promise<any>(() => {}); }
  save?(opts?): Promise<any> { return new Promise<any>(() => {}); }
}

function fillMethods<T>(constructor, mapper: Mapper): T {
  if (constructor.__proto__.name === 'Model') {
    constructor.find = mapper.find;
    constructor.findAll = mapper.findAll;
    constructor.destroy = mapper.destroy;
    constructor.destroyAll = mapper.destroyAll;
    constructor.update = mapper.update;
    constructor.updateAll = mapper.updateAll;
    constructor.prototype.save = function (opts?) { return mapper.create(this, opts); };
  } else {
    const modelClass = (class Test extends Model<T> {
      static find = mapper.find;
      static findAll = mapper.findAll;
      static destroy = mapper.destroy;
      static destroyAll = mapper.destroyAll;
      static update = mapper.update;
      static updateAll = mapper.updateAll;
    } as any);
    modelClass.prototype.save = function (opts?) { return mapper.create(this, opts); };
  }
  return constructor;
}

function buildRefRelations(name: string, jsdataSchema: Schema, deep?: boolean) {
  const options: ModelOptions = { relations: { hasMany: {}, hasOne: {} } };

  Object.keys(jsdataSchema.properties).forEach(key => {
    const isArray = jsdataSchema.properties[key].type === 'array';
    const hasRef = isArray ?
      jsdataSchema.properties[key].items.$ref
      : jsdataSchema.properties[ key ].$ref;
    const mapperName = hasRef ? hasRef.split('/')[ 1 ].toLowerCase() : null;
    const mapper = hasRef ? Jsdata.store.getMapperByName(mapperName) : null;
    if (mapper) {
      if (isArray) {
        options.relations.hasMany[ mapperName ] = {
          localKeys: key,
          localField: key
        };
      } else {
        options.relations.hasOne[ mapperName ] = {
          foreignKey: name + 'Id',
          localField: key
        };
      }

      if (deep) buildRefRelations(name, mapper.schema, deep);
    }
  });

  return options;
}

export function model(options?: ModelOptions): ClassDecorator {
  return function modelDecorator<T>(constructor): T {
    const jsdataSchema = new Schema((constructor as any).$schema);
    const modelName = constructor.name.toLowerCase();
    options = options || buildRefRelations(modelName, jsdataSchema);
    const mapper = Jsdata.store.defineMapper(modelName, { schema: jsdataSchema, ...options });
    return fillMethods<T>(constructor, mapper);
  };
}

@model()
class Address extends Model<Address> {
  @schema({
    type: 'string'
  })
  street: string;

  @schema({
    type: 'string'
  })
  city: string;
}

@model()
class Person extends Model<Person> {
  static anStatic = 0;

  @schema({
    type: 'string'
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    minimum: 18,
  })
  age: number;

  @schema({
    $ref: '/Address'
  })
  address: Address;
}

console.log(Person);
console.log(Person.anStatic);
console.log(Person.find);
console.log(new Person({
  name: 'Filipe',
  age: 19,
  address: { street: 'Rua la', city: 'esteio' }
}));
