/**
 * Configures a Mongoose connection.
 * @module services/mongoose
 * @requires debug
 * @requires colors
 */

import mongoose from 'mongoose';
import createMongooseSchema from 'json-schema-to-mongoose';
import { Debug } from '../utils/debug';
import { validator } from '../utils/schematize';

const debug = new Debug('services:mongodb');

mongoose.connection.once('open', function() {
  debug.log('Connected!');

  mongoose.connection.on('disconnected', function() {
    debug.error('Client disconnected!'.red);
  });

  mongoose.connection.on('error', function(err) {
    debug.error('Error! %s', err.message);
  });
});

export class MongooseService {
  /**
   * Connect the configured Mongoose with a MongoDB Server by it URL.
   * @param {String} mongoUrl The MongoDB URL to connect by.
   * @param {Boolean} enableTryAgain Enable a multiple reconnect if the last fail.
   * @returns {Promise} A promise that resolves when the connection is succesful.
   */
  static connect(mongoUrl, enableTryAgain): Promise<any> {
    debug.info('Connecting with %s', mongoUrl);

    const connectAgain = (err) => {
      if (err) {
        debug.error('Cant connect! Trying again in 30s...\n%s', err.message);
        setTimeout(() => {
          mongoose.connect(mongoUrl, {}, enableTryAgain ? connectAgain : null);
        }, 30000);
      }
    };

    return mongoose.connect(mongoUrl, {})
    // @ts-ignore
      .catch(enableTryAgain ? connectAgain : null);
  }

  static schemaClassToSchema(schemaClass: Function) {
    return createMongooseSchema(validator.schemas, schemaClass['$schema']);
  }
}
