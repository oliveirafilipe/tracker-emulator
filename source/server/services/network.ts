import dns from 'dns';
import util from 'util';
import * as publicIp from 'public-ip';

export class NetworkService {
  static async getPublicIp() {
    return await publicIp.v4();
  }

  static async resolveDomain(domain: string) {
    return (await util.promisify(dns.resolve4)(domain))[0];
  }
}
