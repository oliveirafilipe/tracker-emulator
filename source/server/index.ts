import { App } from './app';
import { config } from './config';

/**
 * The main runnable function that executes the server in the default configuration.
 */
App.configure(config.env);
App.start();
