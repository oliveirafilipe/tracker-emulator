/**
 * Define the Application routes.
 * @module routes
 */
import { Route } from './services/router';

import { ApiCheckController } from './modules/api-check';

const routes: Route[] = [
  { method: 'GET', endpoint: '/', handler: ApiCheckController.status },
  { method: 'WS', topic: 'api_status', access: 'PRIVATE', endpoint: '/', handler: ApiCheckController.status },
];

export default routes;
