import { JavaService } from '../services/java-service';
import { toArrayBuffer, toBufferArray } from '../utils/util';

export class Tracker {
  protected static parser = null;
  protected static javaClassName = null;

  protected static parseJson(json: object): Buffer {
    this.parser = this.parser || JavaService.getJavaInstance().import(this.javaClassName);

     const message = this.parser.encodeSync(
      JavaService.getJavaInstance().newArray('byte', toArrayBuffer(
        new Buffer(JSON.stringify(json))
        )
      ), 'json');

     return toBufferArray(message);

  }
}
