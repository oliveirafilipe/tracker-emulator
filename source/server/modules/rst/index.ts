import { Configuration, Coordinate } from '../../types';
import { tracking } from './packets';
import moment from 'moment';
import { sleep } from '../../utils/system';
import { UdpClientService } from '../../services/udp-client-service';
import { Debug } from '../../utils/debug';
import { Tracker } from '../tracker';

export class Rst extends Tracker {

  private static readonly debug: Debug = new Debug('modules:rst');

  static async sendPosition(configuration: Configuration, coordinates: Array<Coordinate> ) {
    this.javaClassName = 'io.tracktus.raw.rst.standard.Parser';

    for (let i = 0; i < coordinates.length; i++) {
      tracking.header.serialNumber = configuration.deviceId;
      tracking.header.sequenceNumber = i + 1;

      tracking.tracking.eventTimestamp = moment().format('DD-MM-YYYYTHH:mm:ss');
      tracking.tracking.gpsTimestamp = moment().format('DD-MM-YYYYTHH:mm:ss');

      tracking.tracking.ignitionOn = Math.random() < configuration.ignitionOnPercent;
      tracking.tracking.gpsValid = Math.random() < configuration.validPercent;
      tracking.header.status = (Math.random() < configuration.memoryPercent) ? 'LOGGED' : 'ACTUAL';

      tracking.tracking.latitude = coordinates[i].latitude;
      tracking.tracking.longitude = coordinates[i].longitude;
      tracking.tracking.course = coordinates[i].bearing;
      tracking.tracking.speed = i * 3;

      await UdpClientService.sendMessage('rst', this.parseJson(tracking));

      this.debug.log(`(${configuration.deviceId}) RST Sent ${i + 1}/${coordinates.length}`);

      await sleep(configuration.interval * 1000);
    }

    this.debug.log(`(${configuration.deviceId}) RST Finished`);

  }

}
