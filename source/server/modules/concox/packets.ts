export const login = '78780D01{0}002982310D0A';

export const locationData = {
    'header': {
      'messageType': 'LOCATION_DATA',
      'serialNumber': 3,
      'errorCheck': 0
    },
    'dateTime': new Date().toISOString().slice(0, 19),
    'gpsLength': 12,
    'satellites': 12,
    'latitude': 0,
    'longitude': 0,
    'speed': 0,
    'courseStatus': {
      'gpsRealTime': false,
      'gpsPositioned': true,
      'course': 143,
      'accOn': true,
    },
    'mcc': '460',
    'mnc': '0',
    'lac': '10365',
    'cellID': '7864',
    'status': 'OK'
  };

export const alarmData = {
    'header': {
      'messageType': 'ALARM_DATA',
      'serialNumber': 54,
      'errorCheck': 0
    },
    'dateTime': new Date().toISOString().slice(0, 19),
    'gpsLength': 12,
    'satellites': 15,
    'latitude': 23.111755000000002,
    'longitude': 114.40923,
    'speed': 0,
    'courseStatus': {
      'gpsRealTime': false,
      'gpsPositioned': true,
      'course': 2
    },
    'lbsLength': 9,
    'mcc': '460',
    'mnc': '0',
    'lac': '10365',
    'cellID': '8050',
    'terminal': {
      'oilElectricityConnected': false,
      'gpsTrackingOn': true,
      'alarmType': 'NORMAL',
      'chargeOn': true,
      'accHigh': false,
      'defenseActivated': true
    },
    'voltage': 'VERY_HIGH',
    'gsmSignal': 'STRONG_SIGNAL',
    'alarm': 'SOS',
    'language': 'CHINESE',
    'status': 'OK'
  };
