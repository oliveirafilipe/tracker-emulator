import { Configuration, Coordinate } from '../../types';
import { pad, formatString } from '../../utils/util';
import { TcpClientService } from '../../services/tcp-client-service';
import { login, locationData } from './packets';
import { sleep } from '../../utils/system';
import { Debug } from '../../utils/debug';
import { Tracker } from '../tracker';

export class Concox extends Tracker {
  private static readonly debug: Debug = new Debug('modules:concox');

  static async sendPosition(configuration: Configuration, coordinates: Array<Coordinate> ) {
    this.javaClassName = 'io.tracktus.raw.concox.standard.Parser';

    await this.sendLogin(configuration.deviceId);
    for (let i = 0; i < coordinates.length; i++) {

      locationData.dateTime = new Date().toISOString().slice(0, 19);
      locationData.speed = i * 3;
      locationData.header.serialNumber = i + 1;

      locationData.courseStatus.accOn = Math.random() < configuration.ignitionOnPercent;
      locationData.courseStatus.gpsPositioned = Math.random() < configuration.validPercent;
      locationData.courseStatus.gpsRealTime = Math.random() < configuration.memoryPercent;

      locationData.latitude = coordinates[i].latitude;
      locationData.longitude = coordinates[i].longitude;
      locationData.courseStatus.course = coordinates[i].bearing;

      await TcpClientService.sendMessage('concox', this.parseJson(locationData));

      this.debug.log(`(${configuration.deviceId}) Concox Sent ${i + 1}/${coordinates.length}`);

      await sleep(configuration.interval * 1000);
    }

    this.debug.log(`(${configuration.deviceId}) Concox Finished`);
  }

  private static async sendLogin(deviceId: string) {
    deviceId = pad(deviceId, 16);
    await TcpClientService.sendMessage('concox', new Buffer(formatString(login, deviceId), 'hex'));
    await sleep(1500);

  }

}
