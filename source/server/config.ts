import dotenv from 'dotenv';

dotenv.config();

const MONGODB_USER = process.env.MONGODB_USER   || 'dbuser';
const MONGODB_PWD = process.env.MONGODB_PWD     || 'dbuser';
const MONGODB_HOST = process.env.MONGODB_HOST   || 'localhost';
const MONGODB_PORT = process.env.MONGODB_PORT   || '27017';
const MONGODB_NAME = process.env.MONGODB_NAME   || 'app';

export const config = {
  env: {
    mongoURL: `mongodb://${MONGODB_USER}:${MONGODB_PWD}@${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_NAME}`,
    cyberURL: 'postgresql://',
    appLivePort: process.env.LIVE_PORT,
    httpPort: 8080,
    wsPort: 8081,
  },
  testing: {
    mongoURL: 'mongodb://',
    cyberURL: 'postgresql://',
    appLivePort: process.env.LIVE_PORT,
    httpPort: 8080,
    wsPort: 8081,
  }
};
