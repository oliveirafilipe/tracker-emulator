export interface Configuration {

  deviceId: string;
  interval: number;
  ignitionOnPercent: number;
  validPercent: number;
  memoryPercent: number;

}

export interface Coordinate {
  latitude: number;
  longitude: number;
  bearing: number;
}
