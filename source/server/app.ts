/**
 * Define a class that represents the Server Application.
 */

import { WebSocketService } from './services/web-socket';
import { ExpressService } from './services/express';
import { RouterService } from './services/router';
import { JavaService } from './services/java-service';
import { Emulator } from './services/emulator';

import routes from './routes';

// Polyfill to support `async function*` declaration
(<any>Symbol).asyncIterator = (<any>Symbol).asyncIterator || Symbol.for('Symbol.asyncIterator');

export class App {
  static config;
  static configure(config) {
    App.config = config;

    ExpressService.configure({
      appLivePort: config.appLivePort,
      apiRoute: '/api',
      staticRoute: '/app',
    });

    WebSocketService.configure({
      apiRoute: '/api/ws'
    });

    RouterService.setup(routes);

    JavaService.configure();
  }

  static start() {
    Emulator.start();
    // ExpressService.listen(App.config.httpPort);
    // WebSocketService.listen(App.config.wsPort);
  }
}
